const user = {
  // 独立命名空间
  namespaced: true,
  state: {
    userInfo: {
      name: '',
    },
  },
  mutations: {
    SET_NAME: (state, data) => {
      state.userInfo.name = data;
    },
  },
};

export default user;
