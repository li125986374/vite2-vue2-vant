module.exports = {
  extends: ['plugin:vue/essential', 'airbnb-base'],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  plugins: ['vue'],
  rules: {
    // 这里写覆盖vue文件的规则
    'no-console': 'off',
    'no-debugger': 'off',
    'linebreak-style': [0, 'error', 'windows'],
    'import/no-extraneous-dependencies': [0],
    'no-undef': 'warn',
    'max-len': ['error', { code: 500 }],
    'vue/no-mutating-props': ['off'],
    'no-shadow-restricted-names': 'off',
    '@typescript-eslint/no-unused-vars': ['off'],
    '@typescript-eslint/no-explicit-any': ['off'],
    '@typescript-eslint/no-empty-function': ['off'],
    '@typescript-eslint/explicit-module-boundary-types': ['off'],
    '@typescript-eslint/ban-ts-comment': ['off'],
    '@typescript-eslint/ban-types': 'off',
    '@typescript-eslint/no-var-requires': 0,
    'vue/mustache-interpolation-spacing': ['error', 'always'],
    'vue/singleline-html-element-content-newline': 'off',
    'vue/multiline-html-element-content-newline': 'off',
    'vue/html-closing-bracket-newline': 'off',
    // vue 中 script 的缩进
    'vue/script-indent': [
      'error',
      2,
      {
        baseIndent: 0,
        switchCase: 0,
        ignores: [],
      },
    ],
    // vue 中 template 的空格
    'vue/html-indent': [
      'error',
      2,
      {
        attribute: 2,
        baseIndent: 1,
        closeBracket: 0,
        alignAttributesVertically: true,
        ignores: [],
      },
    ],
    // 'vue/max-attributes-per-line': ['error', {
    //   singleline: {
    //     max: 10,
    //     allowFirstLine: true,
    //   },
    //   multiline: {
    //     max: 10,
    //     allowFirstLine: true,
    //   },
    // }],
    'vue/no-v-html': ['off'],
    'vue/require-default-prop': ['off'],
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: [
          'e', // for e.returnvalue
          'ctx', // for Koa routing
          'req', // for Express requests
          'request', // for Express requests
          'response', // for Express responses
          'state', // for vuex state
          'config', // for axios config
          '$root', // for vue-router $route
          '$parent', // for vue-router $route
          '$children', // for vue-router $route
          '$refs', // for vue-router $route
          '$slots', // for vue-router $route
          '$scopedSlots', // for vue-router $route
          '$vnode', // for vue-router $route
          '$attrs', // for vue-router $route
          '$listeners', // for vue-router $route
          '$options', // for vue-router $route
          '$router', // for vue-router $route
          '$route', // for vue-router $route
          '$routerOptions', // for vue-router $route
          '$routeOptions', // for vue-router $route
        ],
      },
    ],
    'import/extensions': ['error', 'always', {
      js: 'never',
      vue: 'never',
    }],
    'import/no-unresolved': ['error', 'always', {
      js: 'never',
      vue: 'never',
    }],
  },
  overrides: [
    {
      files: ['*.vue'],
      rules: {
        'vue/multi-word-component-names': 0,
      },
    },
  ],
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['/@', './src'],
        ],
        extensions: ['.ts', '.js', '.jsx', '.json', '.vue'],
      },
    },
  },
  globals: { // 当访问当前源文件内未定义的变量时，no-undef 规则将发出警告。如果你想在一个源文件里使用全局变量
    uni: true,
    wx: true,
    ROUTES: true,
  },
};
