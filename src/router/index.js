import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

// 注册路由
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/home',
    meta: {
      title: '首页',
    },
    component: Home,
  },
  {
    path: '/home',
    meta: {
      title: '首页',
    },
    component: Home,
  },
  {
    path: '/about',
    meta: {
      title: '关于',
    },
    component: () => import(/* webpackChunkName: "home" */ '../views/About.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
