import Skeleton from './skeleton.vue';

const skeleton = {
  install(Vue) {
    Vue.component('Skeleton', Skeleton);
  },
};
export default skeleton;
