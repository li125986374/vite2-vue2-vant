// 封装页面请求
import request from '@/utils/request';

// 登录验证接口
export const apiGetUser = () => request({ url: '/api/getUser' });

// // 用户数据列表
// export const addUserList = (params) => request({ url: '/users', params });

// // 添加角色的操作
// export const addRole = (data) => request({ url: '/roles', method: 'post', data });
