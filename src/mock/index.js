
export default [
  {
    url: '/api/getUser',
    method: 'get',
    timeout: 5000, // 延迟5s
    response: () => {
      return {
        code: 200,
        message: "ok",
        data: ["tom", "jerry", "jack"]
      };
    }
  }
]