import Vue from 'vue';
import App from './App.vue';
// 引入路由
import router from './router/index';
// 引入Vuex
import store from './store/index';
// 引入自定义vant插件
import './plugins/vant';
// 引入全局组件
import './plugins/globalComponents';

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
