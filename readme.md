## 说明

1. 基于 vite2 和 vue2 全家桶的一个 H5 基础框架
2. 包含 axios，mock，router，vuex，vant，less，postcss-px-to-viewport

## 安装

```
yarn
```

- 如果没有 yarn，先安装 yarn `npm install -g yarn`

## 运行

```
yarn vite
```

## 打包

```
yarn build
```
