// 封装请求
import axios from 'axios';
import { Toast } from 'vant';
import Config from '../config';
// 请求
const http = axios.create({
  baseURL: Config.baseUrl,
  timeout: 6000,
});
// 请求拦截
http.interceptors.request.use((config) => {
  console.log('%c' + 'config', 'background: green; color: #fff;')
  console.log(config)
  // 请求头设置
  const token = localStorage.getItem('token') || '';
  config.headers.Authorization = token;
  return config;
}, (err) => {
  console.log('请求失败')
  console.log(err);
});
// 响应拦截
http.interceptors.response.use((res) => {
  console.log('%c' + 'response', 'background: green; color: #fff;')
  console.log(res)
  // 对响应码的处理
  // eslint-disable-next-line default-case
  // switch (arr.data.meta.status) {
  //   case 200:
  //     // 成功
  //     break;
  //   case 201:
  //     Toast(arr.data.meta.msg);
  //     break;
  //   case 204:
  //     Toast(arr.data.meta.msg);
  //     break;
  //   case 400:
  //     Toast.fail(arr.data.meta.msg);
  //     break;
  //   case 401:
  //     Toast.fail(arr.data.meta.msg);
  //     break;
  //   case 403:
  //     Toast.fail(arr.data.meta.msg);
  //     break;
  //   case 404:
  //     Toast.fail(arr.data.meta.msg);
  //     break;
  //   case 422:
  //     Toast.fail(arr.data.meta.msg);
  //     break;
  //   case 500:
  //     Toast.fail(arr.data.meta.msg);
  //     break;
  // }
  return res.data;
}, (err) => {
  console.log('响应失败')
  console.log(err);
});
// 导出
export default http;
