import { defineConfig } from 'vite';
import { createVuePlugin } from 'vite-plugin-vue2';
import { createStyleImportPlugin, VantResolve } from 'vite-plugin-style-import';
import { viteCommonjs } from '@originjs/vite-plugin-commonjs';
import { viteMockServe } from "vite-plugin-mock";
import path from 'path';

export default defineConfig({
  plugins: [
    createVuePlugin(),
    viteCommonjs(),
    // 按需导入vant的组件样式
    createStyleImportPlugin({
      resolve: VantResolve(),
      libs: [
        {
          libraryName: 'vant',
          esModule: true,
          resolveStyle: (name) => `vant/es/${name}/style/index`,
        },
      ],
    }),
    // 使用mock数据
    viteMockServe({
      mockPath: "src/mock",
      // js开发设置为false
      supportTs: false,
      localEnabled: true, // 是否应用于本地
      prodEnabled: false, // 是否应用于生产
      watchFiles: true, // 监听mock文件变化
      logger: true,
    })
  ],
  base: './',
  // sourcemap: true,
  minify: 'esbuild',
  // 配置别名
  resolve: {
    // 配置路径别名
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
  css: {
    preprocessorOptions: {
      // 这里注意，键名是scss不是sass！一字之差能让你折腾好久
      less: {
        // 这里写你想导入全局scss变量的路径
        // 这里注意只能写相对路径，alias貌似在css中不会生效
        additionalData: "@import './src/assets/less/global.less';",
      },
    },
  },
  server: {
    open: true, // 自动打开浏览器
    hmr: true, // 热更新
    // proxy: {
    //   '/api': {
    //     target: 'http://localhost:3000',
    //     changeOrigin: true,
    //     rewrite: (path) => path.replace(/^\/api/, '')
    //   }
    // }
  },
});
