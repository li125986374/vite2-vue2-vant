import Vue from 'vue';
import Vuex from 'vuex';

import user from './models/user';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: '',
    showSkeleton: true,
  },
  mutations: {
    SET_TOKEN(state, data) {
      state.token = data;
    },
    SET_SKELETON(state, data) {
      state.showSkeleton = data;
    },
  },
  modules: {
    user,
  },
});
